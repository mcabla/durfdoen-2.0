---
naam: Politiek en Filosofisch Konvent
verkorte_naam: PFK
id: pfk
contact: pfk@student.ugent.be
website: https://pfk.ugent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/PFKGent/
themas:
  - politiek en filosofisch
---

Het PFK groepeert de politieke, filosofische en levensbeschouwelijke verenigingen aan de UGent. Dit konvent is de uitgelezen plaats om je aan te sluiten bij een ideologische, filosofische of politieke beweging.
