---
naam: Schamper
id: schamper
verkorte_naam: Schamper
konvent: schamper
website: https://www.schamper.ugent.be
contact: kernredactie@schamper.be
social:
  - platform: facebook
    link: https://www.facebook.com/schampergent/
  - platform: twitter
    link: https://twitter.com/Schamper
  - platform: instagram
    link: https://www.instagram.com/schamper_gent/
themas:
  - cultuur
---

Schamper is anders dan de facultaire verenigingen omdat ze niet allemaal hetzelfde studeren. Ze zijn anders dan de streekgebonden studentenclubs, omdat ze niet allemaal uit dezelfde contreien komen. Ze zijn anders dan politiek-filosofische verenigingen omdat ze niet allemaal dezelfde ideologische mening hebben. Maar ze zijn vooral anders, omdat ze kritische, schampere studentenjournalistiek brengen.
In de eerste plaats is Schamper trots op de journalistiek, literair en esthetisch hoogstaande pareltjes die tweewekelijks, in vijfduizendvoud, aan alle faculteiten en resto's afgeleverd worden.
Daarnaast wagen zij zich ook wel aan activiteiten zoals de ondertussen beruchte Schamperfuif of Schamperquiz.
