---
vraag: Wie wil je ontmoeten?
type: meerkeuze
antwoorden:
  - tekst: Studenten die uit dezelfde streek afkomstig zijn
    vraag: Uit welke streek ben jij afkomstig?
    type: specialCase1
    antwoorden:
      - tekst: TODO
        verenigingen:
          - naam: TODO

  - tekst: Eender wie, onafhankelijk van interesses, studies of andere aspecten
    verenigingen:
      - naam: theepot
      - naam: ideefix

  - tekst: Internationale studenten
    verenigingen:
      - naam: aiesec
      - naam: best
      - naam: esn
      - naam: iaas
      - naam: isag

  - tekst: Ik ben LGBTQI+ student en zou graag andere LGBTQI+ studenten ontmoeten
    verenigingen:
      - naam: vg

  - tekst: Ik heb een migratieachtergond en zou graag mensen met eenzelfde achtergrond als mezelf ontmoeten
    vraag: Je gaf aan een migratieachtergrond te hebben en graag andere studenten te ontmoeten met dezelfde achtergrond. Wat is jouw culturele achtergrond?
    type: dropdown
    antwoorden:
      - tekst: China
        verenigingen:
          - naam: chisag

      - tekst: Armenië
        verenigingen:
          - naam: hayasa

      - tekst: Indonesië
        verenigingen:
          - naam: indosag

      - tekst: Japan
        verenigingen:
          - naam: tnk

      - tekst: Vietnam
        verenigingen:
          - naam: vsag

      - tekst: Sub-Sahara Afrika
        verenigingen:
          - naam: umoja
---
