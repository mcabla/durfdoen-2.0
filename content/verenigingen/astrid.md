---
titel: Home Astrid
id: astrid
naam: Home Astrid
verkorte_naam: Home Astrid
konvent: hk
website: http://astrid.ugent.be/
social:
  - platform: facebook
    link: http://www.facebook.com/groups/167090666639265/
themas:
  -  home
showcase:
  - photo: /assets/logos/AstridA.jpg
  - photo: /assets/logos/AstridB.jpg
  - photo: /assets/logos/AstridC.jpg
---

Gelegen aan het hectische verkeersknooppunt van de Sterre, lijkt de Astrid een oase van rust in het Gentse.
Niets is echter minder waar, een prestatie voor deze home met slechts vijf verdiepingen!
Home Astrid is een relatief kleine vereniging waar iedereen iedereen kent, maar wat hen vooral anders maakt, is dat zij samenleven, een beetje als een familie. De leden koken, feesten, eten, leren, sporten samen en zoveel meer! Ook is er sprake van hulp aan de bewoners en vindt er geregeld een vergadering met de universiteit plaats over eventuele vernieuwingen op de home.
Elk jaar staat er een kersvers, enthousiast praesidium klaar voor de bewoners. Zij organiseren met héél véél goesting spectaculaire fuiven, chille baravonden, onvergetelijke cantussen en een goede dosis aan sport- en cultuuractiviteiten. Er zit dus voor iedereen iets tussen! Deze activiteiten brengen de bewoners van de home samen en zo ontstaan gouden vriendschappen.
De Astridbewoners bezoeken niet enkel hun eigen activiteiten. Met de lintjes om de schouder zorgen ze steeds dat ze goed vertegenwoordigd zijn. Wanneer uiteindelijk de examens voor de deur staan, legt de homeraad zijn activiteiten stil. Studie krijgt dan volop voorrang. Het solarium (bovenste verdiep) is dan ingericht als studeerruimte en blijkt zeer populair te zijn onder de homebewoners.
