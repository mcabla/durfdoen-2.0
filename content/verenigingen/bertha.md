---
titel: Home Bertha de Vriese
id: bertha
naam: Home Bertha de Vriese
verkorte_naam: Home Bertha de Vriese
konvent: hk
website: http://bertha.ugent.be/
social:
  - platform: facebook
themas:
  -  home
showcase:
  - photo: /assets/logos/BerthaA.PNG
  - photo: /assets/logos/BerthaB.jpg
  - photo: /assets/logos/BerthaC.jpg
---

Home Bertha de Vriese, de jongste Home van de universiteit, bevindt zich op campus de Sterre. De kamers zijn studio‘s, er mag immers al eens wat meer luxe en rust zijn ;)
Dat dit de Benjamin van de homes is, weerhoudt onze studentjes er niet van op tijd en stond eens flink uit de bol te gaan.
Het praesidium heeft als doel het leven op Home Bertha aangenaam te maken voor de bewoners door het aanbieden van diverse kleine activiteiten. Dit kan gaan van een quiz tot een baravond of een start-to-run, een film- of serieavond of museumactiviteit, een gezelschapsspellenavond of een pool- of pokertoernooi, enzoverder. Elk semester is er ook steevast de Bertha-fuif, die elk jaar beruchter wordt onder de studenten en keer op keer voor night-to-remember zorgt (en heel wat katers).
Tevens behartigt het praesidium de belangen van de bewoners en fungeert het als een communicatiemiddel tussen de UGent en de studenten.
Het wapenschild heeft de kleuren van de kamers: zalmroze en groen. De kat staat er omdat er - vooral bij zonsondergang - meer katten rond het gebouw zitten dan studenten erin. Verder kan de kater ook gezien worden als verwijzing naar de reputatie van onze fuiven. De twee sterren staan symbool voor de twee blokken die gelegen zijn op campus de Sterre.
