---
titel: Deliria
id: deliria
naam: Deliria
verkorte_naam: Deliria
konvent: sk
contact: deliriagent@gmail.com
website: http://www.deliriagent.be
social:
  - platform: facebook
themas:
  - streek
postcodes:
  - 8480
  - 8820
---

Rood en blauw, het zijn de kleuren waarmee wij Delirianen met veel trots Gent kleuren. Onze voorvaderen kwamen decennia geleden aangewaaid vanuit de regio Torhout/ Lichtervelde/ Ichtegem om met als uitvalsbasis Delirium Tremens de eer van onze oppergod Suzy te verdedigen.
Tegenwoordig sluiten talloze volgeling van ver buiten deze streek zich bij onze rangen aan.
We verzamelen ons traditiegetrouw op maandagavond in ons clubcafé, waar we telkens een fantastische clubavond houden.
Op woensdag organiseren we telkens een spetterende activiteit.
Daarnaast maken we er stilaan ook traditie van om de sportcompetitie van het SK te winnen. Zoals je dus kan lezen: Deliria is zoveel meer dan een gewone studentenclub, bij ons worden er vriendschappen voor het leven gesmeed!
