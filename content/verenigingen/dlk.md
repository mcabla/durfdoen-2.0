---
titel: De Loeiende Koe
id: dlk
naam: De Loeiende Koe
verkorte_naam: De Loeiende Koe
konvent: wvk
contact: dlk@ugent.be
website: https://www.dlk.ugent.be/
social:
  - platform: facebook
    link: http://facebook.com/deloeiendekoe/
  - platform: instagram
    link: http://www.instagram.com/deloeiendekoe/
themas:
  - wetenschap en techniek
  - cultuur
showcase:
  - photo: /assets/logos/dlkA.jpg
  - photo: /assets/logos/dlkB.jpg
  - photo: /assets/logos/dlkC.jpg
---
We schrijven 1995, het ontstaan van een werkgroep op initiatief van de vakgroep Architectuur & Stedenbouw die als dialoog moet dienen tussen leerlingen en leermeesters binnen het architectuuronderwijs.
Een werkgroep die zich doorheen de tijd verder zal ontplooien tot een genootschap der Architectuur: de loeiende koe ziet haar licht. Haar statuut ongedefinieerd. Haar mogelijkheden eindeloos. Het architectuuronderwijs dienend, treedt ze op daar waar het te kort schiet, zo niet poogt ze om de hiaten op te vullen. Een mengvorm van academische sérieux en individuele fascinaties, een werk van liefde, maar ook van overtuiging, dat tegen de achtergrond van een toenemend geglobaliseerde praktijk van architectuurproductie, de intellectuele openheid die de loeiende koe biedt, een mogelijkheid is die gekoesterd moet worden.
Een idee. Het – geloof – in een idee, in iets dat de wereld overstijgt en aan de werkelijkheid ontsnapt. Een idee dat ooit Architectuur was. 
“There was once a dream, that was Architecture. You could only whisper it.” J.R
