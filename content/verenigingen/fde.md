---
naam: Facultaire Studentenraad Dierengeneeskunde
verkorte_naam: DSR
titel: Studentenraad Dierengeneeskunde
id: fde
naam: Studentenraad Dierengeneeskunde
konvent: gsr
logo: /assets/sfeerfotos/LogoDSR.png
themas:
  - engagement
website: http://www.vlaamsdiergeneeskundigekring.be/Studentenraad.php
contact: dsr@student.ugent.be
themas: 
  - engagement
---
De Diergeneeskundige Studentenraad is een facultaire studentenraad die zich inzet voor alle studenten op onze prachtige Faculteit. Wij bestaan voornamelijk uit de jaarverantwoordelijken, die iedereen van zijn/haar jaar natuurlijk wel kent, en de andere studentenvertegenwoordigers (oftewel stuvers). 

Wat onze geweldige jaarverantwoordelijken allemaal voor ons doen (onze vragen beantwoorden, klachten doorgeven/oplossen, leswissels en natuurlijk de kliniekroosters) is alom bekend. Maar wat doen die andere stuvers nu eigenlijk? Zij zitten in de verschillende commissies en raden die onze Faculteit rijk is! Je kan het zo gek maar niet bedenken, of er is wel een commissie voor waar proffen, assistenten en studenten samen zitten om problemen aan te kaarten en oplossingen te bedenken. Ook in centrale raden zoals de Sociale Raad, de Gentse Studentenraad, de Onderwijsraad ed. is er plek voor Diergeneeskunde studenten en is het belangrijk dat wij, op ons eilandje in Merelbeke, niet worden vergeten!
