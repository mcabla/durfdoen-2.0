---
titel: Filologica
id: filologica
naam: Filologica
verkorte_naam: Filologica
konvent: fk
contact: presidium@filologica.be
website: http://www.filologica.be
social:
  - platform: facebook
    link: https://www.facebook.com/Filologica/
  - platform: instagram
    link: https://www.instagram.com/filologica/
themas:
  -  faculteit
---
Filologica is de studentenvereniging voor studenten taal- en letterkunde aan de UGent. De vereniging bestaat sinds 2004 en is sindsdien uitgegroeid tot een hechte familie van Filologen. Wij willen onze leden een onvergetelijke, buitengewone studententijd bezorgen en dit in een vriendschappelijke sfeer waar iedereen zich op zijn gemak voelt. Iedereen is welkom op onze activiteiten!
Zowel voor de feestbeesten als voor de brave studiegerichte student en alle mogelijke mengvormen is er wat wils. Naast onze sport- en reisactiviteiten organiseren wij feestjes, zingen we met volle borst op onze cantussen en trekken we onze fancy kledij aan voor het jaarlijkse Galabal der Filologen. Voor de culinair ingestelde mensen hebben wij eet- en dessertavonden. Ook op studievlak ondersteunen wij Filologen door studiesessies voor eerstejaarsvakken te organiseren en de boekenverkoop aan het begin van het academiejaar te regelen. Verder bieden wij graag een grote portie cultuur aan! Kom bijvoorbeeld eens kijken naar ons eigen toneel, neem deel aan onze quizzen, of geniet van onze culturele benefietavond Woorden en Daden. Een van onze grootste stokpaardjes is ons studentenblad Dilemma, geschreven voor en door studenten taal- en letterkunde. Er valt natuurlijk niets minder dan fantastisch leesvoer te verwachten van de taalrichting bij uitstek!
Nadat we onze boeken uitgelezen hebben en onze taalstructuren ontleed zijn, kun je ons vaak terugvinden in ons stamcafé De Amber, gelegen tegenover onze geliefde faculteit, de Blandijn. Kom zeker eens hallo zeggen en wie weet kom jij er net zoals ons achter dat Filo het ontbrekende puzzelstukje is in de puzzel van het studentenleven.

Als je nog vragen hebt, kan je altijd mailen naar preses@filologica.be
Tot snel!
