---
titel: Kunsthistorische kring
id: khk
naam: Kunsthistorische kring
verkorte_naam: Kunsthistorische kring
konvent: fk
contact: khkgent@gmail.com
website: http://www.khkgent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/Kunst-Historische-Kring-1393134794349013/timeline/
  - platform: instagram
themas:
  -  faculteit
---

De Kunsthistorische Kring is een vereniging voor de studenten van de Archeologie en de Kunstwetenschappen. Of zoals ze zelf zeggen, diegene die bezig zijn met kunst en/of archeologie, maar ook voor die mensen die simpelweg nood hebben aan goed gezelschap. Wil je ook daarbij nog eens op reis gaan met een toffe bende, KHK is the place to be!
