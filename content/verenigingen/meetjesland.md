---
titel: Moeder Meetjesland
id: meetjesland
naam: Moeder Meetjesland
verkorte_naam: Moeder Meetjesland
konvent: sk
website: https://www.moeder-meetjesland.be
social:
  - platform: facebook
    link: https://www.facebook.com/MoederMeetjesland
  - platform: instagram
    link: https://www.instagram.com/meetjesland_gent/
themas:
  - streek
postcodes:
  - 9060
  - 9850
  - 9880
  - 9881
  - 9900
  - 9910
  - 9920
  - 9921
  - 9930
  - 9931
  - 9932
  - 9940
  - 9950
  - 9960
  - 9961
  - 9968
  - 9970
  - 9971
  - 9980
  - 9981
  - 9982
  - 9988
  - 9990
  - 9991
  - 9992
---

Moeder Meetjesland!
Jarenlang is het Meetjesland een ontbrekende schakel geweest in de Gentse studentenwereld. Meetjeslandse studenten merkten op dat ze hun oudeklasgenoten en streekvrienden nauwelijks nog zagen. Sinds 2013 staat Moeder Meetjesland als studentenvereniging echter terug op de kaart, klaar om deze mensen te herenigen. Meetjeslandse studenten vinden elkaar opnieuw in het Gentse en kunnen samen met bekende en onbekende streekgenoten even wegvluchten van de vele cursussen! Je kan ons steeds vinden in de Porter House.
Wij organiseren wekelijks activiteiten voor elke Meetjeslandse en niet-Meetjeslandse student. Gratis vaten, cocktailavonden, bierbowling, barbecue, Meetjesweekend, pokeravond, cantussen,.. maar ook cultuur- (concerten, brouwerijbezoek) en sportactiviteiten zijn er altijd bij. Kortom, voor ieder wat wils én bovendien steeds aan goedkope, studentikoze prijzen!Een topper is onze befaamde Augustijncantus, met Augustijn van onze eigen Meetjeslandse brouwerij Van Steenberghe.
