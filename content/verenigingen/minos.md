---
titel: Minos Gent
id: minos
naam: Minos Gent
verkorte_naam: Minos Gent
konvent: pfk
contact: info@minosgent.gmail.com
website: https://minosgent.com/
social:
  - platform: facebook
    link: https://www.facebook.com/MinosGent
  - platform: instagram
    link: https://www.instagram.com/minosgent
themas:
  -  politiek en filosofisch
  - internationaal
---

Minos? Ja, Minos! Ons doel, als vereniging, is om Europa op politiek en cultureel vlak dichter bij de student te brengen.
Europa is enerzijds het verhaal van integratie tot wat men nu de Europese Unie noemt, maar wordt daarnaast ook gekenmerkt door een zeer grote diversiteit.
Deze diversiteit willen wij ook weergeven in ons gevarieerd aanbod aan activiteiten: lezingen, debatten, café-avonden, culturele activiteiten...
Daarnaast zijn we nog steeds een studentenvereniging, wat maakt dat we het belangrijk vinden om dicht bij de student te staan en zo laagdrempelig mogelijk te werken. Meer weten? Neem dan zeker een kijkje op onze website!
