---
titel: Atelier Modelstudies
id: modelstudies
naam: Atelier Modelstudies
verkorte_naam: Atelier Modelstudies
konvent: kultk
themas:
  -  cultuur
contact: modelstudies@student.ugent.be
showcase:
  - photo: /assets/logos/AtelierA.png
  - photo: /assets/logos/AtelierB.png
  - photo: /assets/logos/AtelierC.png

---
Een boeiend en gevarieerd beeldend atelier waarin je tekent en schildert naar naaktmodel, dat is wat het Schildersatelier jou ieder semester te bieden heeft in de vorm van een lessenreeks modelstudies. Iedere les nemen we een thema en kunstenaar onder de loep, waarbij we telkens andere technieken en materialen hanteren, met veel ruimte voor experiment. Beginner of gevorderde, iedereen is welkom en krijgt begeleiding op maat van onze professionele docenten. De lessen vinden plaats op maandag, dinsdag, woensdag of donderdag in ons atelier te Home Fabiola (Stalhof 4)
