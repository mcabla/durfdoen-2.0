---
titel: Prometheus
id: prometheus
naam: Prometheus
verkorte_naam: Prometheus
konvent: kultk
logo: /assets/sfeerfotos/logoprometheus.jpg
contact: prometheusgent@gmail.com
website: https://prometheusgent.wordpress.com/
social:
  - platform: facebook
    link: https://www.facebook.com/PrometheusGent/
themas:
  - cultuur
showcase:
  - photo: /assets/logos/PrometheusA.jpg
  - photo: /assets/logos/PrometheusB.JPG
  - photo: /assets/logos/PrometheusC.JPG
---

Prometheus is de studentenvereniging voor creatief schrijven in het Engels. Wij verwelkomen iedereen die graag tovert met wervelende woorden, verrassende verhalen schrijft of schitterende poëzie produceert. Om het creatieve vuur aan te wakkeren organiseren we twee-wekelijkse Scribble-sessies, waar wij de prompts voorzien en je aan het schrijven krijgen. Daarnaast nodigen we op regelmatige basis schrijvers uit die komen vertellen over hun ervaringen en hun werk. Altijd al gedroomd van een publicatie? Wij geven jaarlijks een magazine uit, Simile, met een wisselend thema, geredigeerd en geschreven door studenten. We schrijven niet alleen, maar organiseren jaarlijks een Reading Marathon, waar we 12 uur lang een verhaal (of verhalen) voorlezen en, samen ontspannen voor de examens doe we ook.  Elk semester is er een Prometheus Unwinds, waar we samen pizza eten en spelletjes spelen. Daarnaast voorzien we op al onze activiteiten gratis koffie en thee. Wat moet een mens meer hebben? 
