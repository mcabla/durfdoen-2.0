---
titel: "'t Stropke"
id: stropke
naam: "'t Stropke"
verkorte_naam: "'t Stropke"
konvent: sk
website: http://tstropke.be/
social:
  - platform: instagram
    link: https://www.instagram.com/tstropkeleeft/
  - platform: facebook
    link: https://www.facebook.com/tstropke/
themas:
  - streek
postcodes:
  - 9000
  - 9030
  - 9031
  - 9032
  - 9040
  - 9041
  - 9042
  - 9050
  - 9051
  - 9052
  - 9070
  - 9080
  - 9090
  - 9185
  - 9800
  - 9820
  - 9830
  - 9831
  - 9840
---

't Stropke is dé studentenclub voor studenten in Gent, afkomstig uit Gent zelf. Met Gent bedoelen we groot Gent. Ben je van de omstreken (Wachtebeke, Lochristi, Destelbergen, Merelbeke, Melle, De Pinte, Sint-Martens-Latem en Deinze) ? Dan hoor je ook bij ons!
Schransen en drinken, maar ook sport en cultuur staan bij ons centraal. Bovenal zijn we een bende vrienden en een club die banden voor het leven wil scheppen, en de studententijd in al zijn deugden en plichten in ons schoon Gent wil verrijken.
