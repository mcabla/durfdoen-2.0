---
naam: StuFF
verkorte_naam: StuFF
titel: Studentenraad Faculteit Farmaceutische Wetenschappen
id: stuff
naam: Studentenraad Faculteit Farmaceutische Wetenschappen
konvent: gsr
website: https://stuff.ugent.be/
social:
  - platform: facebook
    link: https://www.facebook.com/StudentenraadFaculteitFarmacie
themas:
  -  engagement
---

StuFF is de Studentenraad van de Faculteit Farmaceutische Wetenschappen aan de Universiteit Gent. Enerzijds is StuFF een facultair aanspreekpunt voor studenten en een overlegplatform tussen studentenvertegenwoordigers in facultaire raden en commissies, alsook verenigt ze de jaarverantwoordelijken. Anderzijds wordt er ook sterk ingezet op de centrale vertegenwoordiging zoals binnen de Gentse Studentenraad en op Vlaams niveau binnen het Vlaams Farmaceutisch Studenten Overleg.
De inzet van studentenvertegenwoordigers op de Faculteit Farmaceutische Wetenschappen bestaat uit het verdedigen van de belangen van de studenten en het aankaarten van bestaande problematiek binnen de verschillende instanties. Op deze manier hebben studenten ook inspraak in het beleid van de faculteit en haar aangeboden opleidingen. Het mee bewaken van kwaliteitsvol onderwijs en samenwerken tussen bestaande organen zijn dan ook onze kerntaken.
