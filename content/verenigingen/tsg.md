---
titel: "'t Zal Wel Gaan"
id: tsg
naam: "'t Zal Wel Gaan"
verkorte_naam: "'t Zal Wel Gaan"
konvent: pfk
contact: info@tzalwelgaan.be
website: http://tzalwelgaan.be/
social:
  - platform: instagram
    link: https://www.instagram.com/tzalwelgaan
  - platform: facebook
    link: https://www.facebook.com/tzalwelgaan
  - platform: twitter
    link: https://twitter.com/tzalwelgaan
themas:
  - politiek en filosofisch
---

Het Taalminnend Studenten Genootschap 't Zal Wel Gaan werd in 1852 opgericht te Gent onder de leuze 'Klauwaard en Geus'. 't Zallers zijn de vrijdenkers van de UGent - ze staan vooral bekend om hun tegendraad-sheid, hun kritische geest en ja, hun historische strijd tegen de tsjeven.
Aangezien 't Zal de oudste nog bestaande studentenvereniging is van Vlaanderen, is er een rijke geschiedenis en zijn er talloze tradities die zich - soms in eigentijdse vorm - nog steeds manifesteren.
Ze put hiervoor uit de vrijzinnige, orangistische, liberale, taalminnende, humanistische, progressieve, feministische, antifascistische en anarchistische idealen die haar leden sinds 1852 in wisselende golven hebben uitgedragen.
Vandaag verenigt 't Zal studenten die over de hokjes van religie en politieke ideologie willen kijken. Elke dinsdagavond komen we samen voor lezingen en debatten bij kaarslicht onder de leuze 'niemands knecht, niemands meester!' of ook wel 'nie geluve!'.
Vaak nodigen we een spreker uit die een bepaald gespreksthema inleidt, waarna het grootste deel van de avond voor discussie gereserveerd is. In donkere 19e eeuwse kamertjes steken we de draak met kruisbeelden en alles wat de vrije geest kan beknotten, en dromen we van werkelijk vrije seks - wat is dat eigenlijk? We filosoferen dus over onderwerpen van actuele, politieke en levensbeschouwelijke aard, de ene hand slaand op tafel en in de andere een goede trappist.
Je vindt hier communistische individualisten, porno-verzamelende feministen, islamofobe libertairen, gewichtheffende poëten, linkshandige economisten en ga zo maar door.
