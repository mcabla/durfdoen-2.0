---
titel: Vlaamse Biomedische Kring
id: vbk
naam: Vlaamse Biomedische Kring
verkorte_naam: Vlaamse Biomedische Kring
konvent: fk
website: https://www.vlaamsebiomedischekring.be/
contact: vbkgent@gmail.com
social:
  - platform: facebook
    link: https://www.facebook.com/vlaamsebiomedischekring/
themas:
  - faculteit
---

V.B.K. simply the best! Voor alle studenten Biomedische wetenschappen organiseren wij tal van beestelijke feestjes in de Point Final, tevens ons kringcafe waar tal van promo's doorgaan. Ook hebben wij ons zalige galabal samen met VLAK. Naast feestjes doen we ook toffe sportactiviteiten, cultuuractiviteiten enz. Met ons zal je de leukste studententijd tegemoet gaan, twijfel dus niet om lid te worden van onze familie, je zal er zeker geen spijt van hebben!
