---
titel: Veto Gent
id: veto
naam: Veto Gent
verkorte_naam: Veto Gent
konvent: fk
website: https://vetogent.fkgent.be/
contact: presidium@vetogent.be
social:
  - platform: facebook
    link: https://www.facebook.com/vetogentonline/
  - platform: instagram
    link: https://www.instagram.com/vetogent/
themas:
  -  faculteit
showcase:
  - photo: /assets/logos/VetoA.jpg
  - photo: /assets/logos/VetoB.jpg
  - photo: /assets/logos/VetoC.jpg
---
Veto Gent is de vereniging voor alle studenten Toegepaste Taalkunde. Wij helpen je niet alleen bij je studies, maar we organiseren ook leuke feestjes. Je vindt ons elke dinsdag in Cuba, ons stamcafé. Onze cocktailavonden staan erom bekend legendarisch en onvergetelijk te zijn, hoewel veel mensen zich de volgende dag weinig kunnen herinneren. Veto Gent is ook gewoon een aangename vereniging waar iedereen welkom is.
