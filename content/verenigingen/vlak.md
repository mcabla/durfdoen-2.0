---
titel: Vlaamse Logopedische en Audiologische Kring
id: vlak
naam: Vlaamse Logopedische en Audiologische Kring
verkorte_naam: Vlaamse Logopedische en Audiologische Kring
konvent: fk
website: http://www.vlak.be
contact: vlak@vlak.be
social:
  - platform: instagram
    link: https://www.instagram.com/vlak_gent/
  - platform: facebook
    link: https://www.facebook.com/VlaamseLogopedischeenAudiologischeKring/
themas:
  -  faculteit
---
VLAK (Vlaamse Logopedische Audiologische Kring) is een Gentse studentenvereniging voor de studenten logopedische en audiologische wetenschappen van de Universiteit van Gent.
Wij zijn opgericht door T. Proost in 2001 en worden elk jaar wat groter.
Wij organiseren talloze activiteiten doorheen het academiejaar: van cantussen tot workshops zelfverdediging. Voor ieder wat wils. Natuurlijk zijn andere studenten zeker welkom.
