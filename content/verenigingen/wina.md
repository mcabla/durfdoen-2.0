---
titel: WiNA
id: wina
naam: WiNA
verkorte_naam: WiNA
konvent: fk
themas:
  -  faculteit
contact: wina@student.ugent.be
website: https://wina-gent.be/
---

WiNA is de faculteitskring van en voor de studenten Wiskunde, Informatica en Fysica & Sterrenkunde aan de Universiteit Gent.
